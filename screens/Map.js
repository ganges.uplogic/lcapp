import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import MapView from 'react-native-maps';  
import { Marker } from 'react-native-maps';  

export default function Map() {
  return (
    <View style={{flex: 1}}>
    <MapView  
    style={styles.map}  
    showsUserLocation={false}  
    zoomEnabled={true}  
    zoomControlEnabled={true}  
    initialRegion={{  
      latitude: 9.915498233761472,     
      longitude: 78.12412185749965, 
      latitudeDelta: 0.2565,   
      longitudeDelta: 0.1121,  
    }}>  
    <Marker  
      coordinate={{ latitude: 9.920796017182488, longitude: 78.11930459187018 }}  
      title={"Your Location"}  
      description={"Madurai"}  
    />  
  </MapView>  
    </View>
  );
}
const styles = StyleSheet.create({
    map: {
      ...StyleSheet.absoluteFillObject,
    },
});
