import { StyleSheet, Text, View } from 'react-native'
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Register from './Register';
import HomeFile from './HomeFile';

export default function AuthNav() {
    const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator initialRouteName="Register">
        <Stack.Screen name="Register" component={Register} options={{headerShown:false}} />
        {/* <Stack.Screen name="NewReg" component={NewReg} options={{headerShown:false}} /> */}
        <Stack.Screen name='Home' component={HomeFile} options={{headerShown:false}} />
    </Stack.Navigator>
  )
}

const styles = StyleSheet.create({})