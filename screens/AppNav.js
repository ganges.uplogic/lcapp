import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Register from './Register';
import HomeFile from './HomeFile';
import NewReg from '../pages/NewReg';
import AuthNav from './AuthNav';
import SideBar from './SideBar';
import CustomDraw from './CustomDrawer';
import Icon from 'react-native-vector-icons/Ionicons';
import Map from './Map';

export default function AppNav() {
  const Drawer = createDrawerNavigator();
  return (
    <Drawer.Navigator
      screenOptions={{headerShown: false, drawerLabelStyle: {marginLeft: -20}, drawerActiveBackgroundColor: '#0aada8', drawerActiveTintColor: '#fff', drawerInactiveTintColor: '#333'}}
      drawerContent={props => <CustomDraw {...props} />}>
      <Drawer.Screen
        name="Home"
        component={AuthNav}
        options={{
          headerShown: false,
          drawerIcon: ({color}) => <Icon name="home" size={22} color={color} />,
        }}
      />
      <Drawer.Screen
        name="About"
        component={SideBar}
        options={{
          headerShown: false,
          drawerIcon: ({color}) => (
            <Icon name="journal-sharp" size={22} color={color} />
          ),
        }}
      />
      <Drawer.Screen
        name="Map"
        component={Map}
        options={{
          headerShown: false,
          drawerIcon: ({color}) => <Icon name="map" size={22} color={color} />,
        }}
      />
    </Drawer.Navigator>
  );
}

const styles = StyleSheet.create({});
