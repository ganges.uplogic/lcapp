import { View, Text, Image } from 'react-native'
import React from 'react'

export default function ImageCom({data}) {
  return (
    <View>
     <Image source={data.image} style={{height: 150, width: 250, borderRadius: 8}} />
    </View>
  )
}