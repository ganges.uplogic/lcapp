import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {Avatar, ScrollView} from 'native-base';
import IconMtl from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/Ionicons';

export default function CustomDraw(props) {
  return (
    <View style={{flex: 1}}>
      <DrawerContentScrollView
        // contentContainerStyle={{backgroundColor: '#0aada8'}}
        {...props}>
        <View
          style={{
            marginVertical: 5,
            borderRadius: 4,
            backgroundColor: '#0aada8',
            paddingHorizontal: 30,
            paddingVertical: 30,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Avatar
            size="lg"
            source={{
              uri: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80',
            }}
          />
          <View style={{marginLeft: 20}}>
            <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 18}}>
              Melora
            </Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon
                style={{marginRight: 4, color: '#ff1744'}}
                name="md-location-sharp"
                size={18}
                color="#000"
              />
              <Text style={{color: '#fff', fontSize: 12, fontWeight: 'bold'}}>
                MallDevs
              </Text>
            </View>
          </View>
        </View>

        <DrawerItemList {...props} />
      </DrawerContentScrollView>
      <View style={{ backgroundColor: '#0aada8', paddingHorizontal: 25}}>
        <TouchableOpacity onPress={() => {}} style={{paddingVertical: 25}}>
          <View style={{flexDirection: 'row'}}>
            <IconMtl
              style={{marginRight: 4, color: '#ff1744'}}
              name="logout"
              size={18}
            />
            <Text style={{color: '#fff', fontWeight: 'bold'}}>Logout</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}
// AIzaSyCn6xoYs6pvlISBc8zLYw98eru_JmNvSLg