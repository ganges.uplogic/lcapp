import { View, Text, SafeAreaView, StyleSheet, ScrollView, TextInput } from 'react-native'
import React, { useState } from 'react';
import {Formik} from 'formik';
import * as yup from 'yup';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function Register({navigation}) {
    const [FormVal, setFormVal] = useState({
        email: '',
        password: '',
        confirm_password: '',
      });
    
      const loginValidationSchema = yup.object().shape({
        email: yup
          .string()
          .email('Please enter valid email')
          .required('Email Address is Required'),
        password: yup
          .string()
          .min(8, ({min}) => `Password must be at least ${min} characters`)
          .required('Password is required'),
        name: yup.string().required('Name is Required'),
      });
  return (
    <View style={styles.View}>
      <Text style={styles.text}>Registration</Text>
      <Text style={styles.text1}>Please register down below</Text>
      <Formik
        validationSchema={loginValidationSchema}
        initialValues={{email: '', password: '', name: ''}}
        onSubmit={(values) => {
          console.log(values)
          navigation.navigate('Home');
        }
        }>
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <View style={styles.box}>
            <View
             style={styles.field}
            //   style={{
            //     borderWidth: 2,
            //     marginTop: 20,
            //     paddingHorizontal: 10,
            //     marginHorizontal: 35,
            //     borderColor: '#000',
            //     borderRadius: 25,
            //     height: 50,
            //   }}
              >
              <View
                style={styles.inField}
                // style={{
                //   flexDirection: 'row',
                //   alignItems: 'center',
                //   paddingHorizontal: 12,
                // }}
                >
                  <Icon name="user" size={20} color="#000" />
                <TextInput
                  placeholder="Name"
                  placeholderTextColor={'#000'}
                  onChangeText={handleChange('name')}
                  onBlur={handleBlur('name')}
                  // onChangeText={value => setFormVal({...FormVal, email: value})}
                  style={styles.input}
                />
              </View>
              {errors.name && touched.name && (
                <Text style={styles.errorText}>{errors.name}</Text>
              )}
            </View>
            <View
              style={styles.field}>
              <View style={styles.inField}>
                  <MaterialIcon
                    name="local-post-office"
                    size={20}
                    color="#000"
                  />
                <TextInput
                  placeholder="Email"
                  onChangeText={handleChange('email')}
                  onBlur={handleBlur('email')}
                  placeholderTextColor={'#000'}
                  style={styles.input}
                />
              </View>
              {errors.email && touched.email && (
                <Text style={styles.errorText}>{errors.email}</Text>
              )}
            </View>
            <View style={styles.field}>
              <View style={styles.inField}>
                  <MaterialIcon name="security" size={20} color="#000" />
                <TextInput
                  secureTextEntry
                  placeholder="Password"
                  placeholderTextColor={'#000'}
                  style={styles.input}
                  onBlur={handleBlur('password')}
                  onChangeText={handleChange('password')}
                />
              </View>
              {errors.password && touched.password && (
                <Text style={styles.errorText}>{errors.password}</Text>
              )}
            </View>
            <View style={styles.btn}>
              <Text
                style={styles.btnTxt}
                onPress={handleSubmit}>
                REGISTER
              </Text>
            </View>
          </View>
        )}
      </Formik>
      <Text style={styles.line}>Or Continue With</Text>
      {/* <Image
        style={{width: '200', height: '50'}}
        source={require('../pages/images/fb.png')}
      /> */}
    </View>
    // <SafeAreaView style={styles.container}>
    //     <ScrollView style={styles.ScrollView}>
    //         <View style={styles.View}>
    //             <Text style={styles.text}>Registration</Text>
    //         </View>
    //     </ScrollView>
    // </SafeAreaView>
  )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    ScrollView: {
        // flex: 1,
        backgroundColor: 'white'
    },
    View: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FDFCFF'
      },
    text: {
       color: 'black',
       fontSize: 22,
       fontWeight: 'bold',
       marginBottom: 10
    },
    text1: {
       color: 'gray',
       fontSize: 14,
       marginBottom: 20
    },
    box: {
        width: 350
    },
    field: {
        marginTop: 20,
        paddingHorizontal: 10,
        marginHorizontal: 35,
        borderRadius: 10,
        height: 50,
        backgroundColor: '#F8FAFB'
    },
    inField: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 12,
    },
    input: {
        paddingHorizontal: 10, 
        color: '#000', 
        flex: 1
    },
    btn: {
        paddingVertical: 14,
        marginTop: 20,
        paddingHorizontal: 10,
        marginHorizontal: 35,
        backgroundColor: '#0aada8',
        borderRadius: 10,
    },
    btnTxt: {
        color: '#fff', 
        fontWeight: 'bold', 
        fontSize: 16, 
        width: '100%', 
        textAlign: 'center'
    },
    line: {
        fontSize: 12,
        marginTop: 20,
        color: '#000'
    },
    errorText: {
      fontSize: 10,
      color: 'red',
      textAlign: 'right',
      marginTop: 2,
    },
    errorIcon: {
      textAlign: 'right',
    },
  })