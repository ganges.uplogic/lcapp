import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {Avatar, ScrollView} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import Carousel from 'react-native-snap-carousel';
import {SlideData} from './model/SlideData';
import ImageCom from './component/ImageCom';
import {windowWidth} from './utils/Dimensions';

export default function HomeFile({navigation}) {
  const renderSlide = ({item, index}) => {
    return <ImageCom data={item} />;
  };
  return (
    // <View style={styles.View}>
    //   <Text style={styles.Text}>HomeFile</Text>
    // </View>
    // <ScrollView w={["200", "300"]} h={"80"}>
    <View style={styles.View}>
      <View style={styles.smView}>
        <Text style={styles.logo}>Hi, Melora</Text>
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Avatar
            source={{
              uri: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80',
            }}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.inputBox}>
        <Icon
          style={{marginLeft: 8}}
          name="md-search-outline"
          size={20}
          color="#000"
        />
        <TextInput
          placeholder="Search"
          placeholderTextColor={'#000'}
          style={styles.input}
        />
      </View>
      <View style={styles.upBox}>
        <Text style={{color: '#000', fontSize: 16, fontWeight: 'bold'}}>
          Popular Places
        </Text>
        <TouchableOpacity>
          <Text style={{color: '#0aada8', fontWeight: 'bold'}}>See More</Text>
        </TouchableOpacity>
      </View>
      <View style={{paddingHorizontal: 10, marginTop: 5}}>
        <Carousel
          // ref={(c) => { this._carousel = c; }}
          data={SlideData}
          renderItem={renderSlide}
          sliderWidth={windowWidth - 20}
          itemWidth={250}
          autoplay={true}
          loop={true}
        />
      </View>
    </View>
    // </ScrollView>
  );
}

const styles = StyleSheet.create({
  View: {
    flex: 1,
    backgroundColor: '#fff',
  },
  Text: {
    color: '#000',
    fontSize: 20,
  },
  smView: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    height: 60,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginVertical: 10,
  },
  logo: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#000',
  },
  inputBox: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#c6c6c6',
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 1,
    marginHorizontal: 20,
    borderRadius: 18,
    height: 45
  },
  input: {
    paddingLeft: 8,
    color: '#000',
    width: '90%',
  },
  upBox: {
    marginVertical: 15,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
