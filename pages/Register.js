import React, {useState} from 'react';
import {Formik} from 'formik';
import * as yup from 'yup';
// import type {Node} from 'react';
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
} from 'react-native';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/FontAwesome';

const Register = () => {
  const loginValidationSchema = yup.object().shape({
    email: yup
      .string()
      .email('Please enter valid email')
      .required('Email Address is Required'),
    password: yup
      .string()
      .min(8, ({min}) => `Password must be at least ${min} characters`)
      .required('Password is required'),
    name: yup.string().required('Name is Required'),
  });
  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
    <View style={styles.container}>
    <Image
          style={styles.imgs}
          source={require('./images/alone1.jpg')}
        />
        <Text
          style={{
            color: '#000',
            alignSelf: 'center',
            fontSize: 30,
            fontWeight: 'bold',
            marginTop: 20,
          }}>
          Join Our Community
        </Text>
        <Text
          style={{
            color: '#000',
            textAlign: 'center',
            fontSize: 14,
            marginHorizontal: 20,
            marginTop: 20,
          }}>
          You want a very diverse range of illustrations, have a look at this
          gallery and the number of vector graphics depicting people.
        </Text>
        <Formik
          validationSchema={loginValidationSchema}
          initialValues={{email: '', password: '', name: ''}}
          onSubmit={values => console.log(values)}>
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <>
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 20,
                  paddingHorizontal: 10,
                  marginHorizontal: 35,
                  borderColor: '#375963',
                  borderRadius: 25,
                  height: 50,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: 12,
                  }}>
                  <Icon name="user" size={20} color="#375963" />
                  <TextInput
                    placeholder="Name"
                    placeholderTextColor={'#375963'}
                    onChangeText={handleChange('name')}
                    onBlur={handleBlur('name')}
                    // onChangeText={value => setFormVal({...FormVal, email: value})}
                    style={{paddingHorizontal: 10, color: '#375963', flex: 1}}
                  />
                </View>
                {errors.name && touched.name && (
                  <Text style={styles.errorText}>{errors.name}</Text>
                )}
              </View>
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 16,
                  paddingHorizontal: 10,
                  marginHorizontal: 35,
                  borderColor: '#375963',
                  borderRadius: 25,
                  height: 50,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: 12,
                  }}>
                  <MaterialIcon
                    name="local-post-office"
                    size={20}
                    color="#375963"
                  />
                  <TextInput
                    placeholder="Email"
                    onChangeText={handleChange('email')}
                    onBlur={handleBlur('email')}
                    placeholderTextColor={'#375963'}
                    style={{paddingHorizontal: 10, color: '#375963', flex: 1}}
                  />
                </View>
                {errors.email && touched.email && (
                  <Text style={styles.errorText}>{errors.email}</Text>
                )}
              </View>
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 16,
                  paddingHorizontal: 10,
                  marginHorizontal: 35,
                  borderColor: '#375963',
                  // borderColor: (errors.password) ? 'red' : '#375963',
                  borderRadius: 25,
                  height: 50,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: 12,
                  }}>
                  <MaterialIcon name="security" size={20} color="#375963" />
                  <TextInput
                    secureTextEntry
                    placeholder="Password"
                    placeholderTextColor={'#375963'}
                    style={{paddingHorizontal: 10, color: '#375963', flex: 1}}
                    onBlur={handleBlur('password')}
                    onChangeText={handleChange('password')}
                  />
                </View>
                {errors.password && touched.password && (
                  <Text style={styles.errorText}>{errors.password}</Text>
                )}
              </View>
              <View
                style={{
                  paddingVertical: 14,
                  marginTop: 20,
                  paddingHorizontal: 10,
                  marginHorizontal: 35,
                  backgroundColor: '#375963',
                  borderRadius: 25,
                }}>
                <Text
                  style={{
                    color: '#fff',
                    fontWeight: 'bold',
                    fontSize: 20,
                    width: '100%',
                    textAlign: 'center',
                  }}
                  onPress={handleSubmit}>
                  Join Us
                </Text>
              </View>
            </>
          )}
        </Formik>
  </View>
  </ScrollView>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
      },
    imgs: {width: '100%', height: '40%'},
      text: {
        color: "white",
        fontSize: 42,
        lineHeight: 84,
        fontWeight: "bold",
        textAlign: "center",
        backgroundColor: "#000000c0"
      },
      
  });

export default Register;