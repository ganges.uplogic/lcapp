// import {Center, Heading, ScrollView, View} from 'native-base';
import React from 'react';
import {ViewComponent, StyleSheet} from 'react-native';
import {
  VStack,
  Text,
  HStack,
  Button,
  IconButton,
  Icon,
  NativeBaseProvider,
  Center,
  Box,
  StatusBar,
  Container,
  Header,
  View,
  ScrollView,
} from 'native-base';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
// import { Header } from 'react-native/Libraries/NewAppScreen';

const Home = ({navigation}) => {
  return (
    // <>
    //   <Text>Home</Text>
    //   <Button
    //     onPress={() => navigation.navigate('Register')}
    //     title="Learn More"
    //     color="#841584"
    //     accessibilityLabel="Learn more about this purple button"
    //   />
    // </>
    <>
      {/* <Center mt="3" mb="4">
        <Heading fontSize="xl">Cyan</Heading>
      </Center> */}
      <StatusBar bg="#3700B3" barStyle="light-content" />
      <Box safeAreaTop bg="violet.600" />
      <HStack bg="violet.600" px="1" py="3" justifyContent="space-between" alignItems="center" w="100%">
        <HStack alignItems="center">
          
          <IconButton onPress={() => navigation.navigate('NewReg')} icon={<MaterialIcon name="menu" size={20} color="white" />} />
          <Text color="white" fontSize="20" fontWeight="bold">
            Home
          </Text>
        </HStack>
        <HStack>
          <IconButton icon={<MaterialIcon name="favorite" size={20} color="white" />} />
          <IconButton icon={<MaterialIcon name="search" size={20} color="white" />} />
          <IconButton icon={<MaterialIcon name="more-vert" size={20} color="white" />} />
        </HStack>
      </HStack>
      {/* <ScrollView w={['400', '300']} h="80">
        <View style={styles.cont}>
          <View
            style={{
              backgroundColor: '#375963',
              height: 90,
              borderBottomColor: '#757575',
              // alignItems:'',
              justifyContent: 'space-around'
            }}>
            <View style={{flexDirection: 'row'}}>
            <MaterialIcon name="menu" size={20} color="white" />
            </View>
          </View>
        </View>
      </ScrollView> */}
    </>
  );
};

const styles = StyleSheet.create({
  cont: {
    width: '100%',
    backgroundColor: '#fff',
    flex: 1,
  },
});

export default Home;
