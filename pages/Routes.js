import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Home from './Home'
import NewReg from './NewReg'

const Routes = () => {
  return (
    <Router>
      <Scene key = "root">
         <Scene key = "home" component = {NewReg} title = "NewReg" initial = {true} />
         <Scene key = "about" component = {Home} title = "Home" />
      </Scene>
   </Router>
  )
}

export default Routes