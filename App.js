/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
// import type {Node} from 'react';
import {
  StyleSheet
} from 'react-native';

import { Router, Scene } from 'react-native-router-flux'
import NewReg from './pages/NewReg';
import Home from './pages/Home';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NativeBaseProvider } from 'native-base';
import Register from './screens/Register';
import FieldsCom from './screens/component/FieldsCom';
import HomeFile from './screens/HomeFile';
import AuthNav from './screens/AuthNav';
import AppNav from './screens/AppNav';
import {enableLatestRenderer} from 'react-native-maps';

enableLatestRenderer();

const App = () => {
  const Stack = createNativeStackNavigator();
  
  return (
    // <>
    //    <NewReg />
    // {/* // </View> */}
    // </>
    <NativeBaseProvider>
      <NavigationContainer>
         {/* <AuthNav /> */}
         <AppNav />
      </NavigationContainer>
    </NativeBaseProvider>
    // <>
    // <Register />
    // {/* <FieldsCom /> */}
    // </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  
});

export default App;
